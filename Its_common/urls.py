from django.conf.urls import url,patterns,include
from django.contrib import admin
import myapp.views
from django.views.generic import TemplateView
	
urlpatterns = [
url(r'^admin/',include(admin.site.urls)),
url(r'^hello/',myapp.views.hello),
#url(r'^stlogin/',myapp.views.stlogin, name='stlogin'),
url(r'^login/$',myapp.views.mylogin, name='loggedin'),

    url(r'^register/', myapp.views.myregister, name='register'),
    url(r'^logout/', myapp.views.mylogout, name='logout'),

]
#url(r'^simpleemail/(?P<emailto>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/',myapp.views.sendSimpleEmail , name = 'sendSimpleEmail'),]