from django.shortcuts import render , redirect
import myapp.models
from django.http import HttpResponse
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,

    )
from .forms import UserLoginForm, UserRegisterForm


def hello(request):

	return render(request, "login.html", {})

def mylogin(request):
    print(request.user.is_authenticated())
    next = request.GET.get('next')
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return render(request,"loggedin.html")
    return render(request, "form.html", {"form":form, "title": title})

def myregister(request):
    print(request.user.is_authenticated())
    next = request.GET.get('next')
    title = "Register"
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return render(request,"form.html")

    context = {
        "form": form,
        "title": title
    }
    return render(request, "form.html", context)

      
def mylogout(request):
  logout(request)
  return render(request, 'index.html')