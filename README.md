## 1. Pre-requisites ##
* 	Python
* 	Python-pip
* 	Django
*       Virtual Enviroment
*       Phpmyadmin
*       MySQL

## 2.Steps ##
 
### Install Python if not already installed ###
    'sudo apt-get update'
    'sudo apt-get install python'
### Check Version ###
    'python -V'
### Install pip ###
    'sudo apt-get install python-pip' --for version2
    'sudo apt-get install python3-pip' --for version3
### Install Virtual Enviroment ###
    'sudo apt-get install virtualenv'
### Install Django ###
    'sudo pip install Django'
    'sudo pip3 install Django' --Pip3

### Install MySQL if not already installed ###
    'sudo apt-get update'
    'sudo apt-get install mysql'
    'apt-get install python-dev libmysqlclient-dev' --install developement packages
    'pip install MySQL-python'

## 3. Running the Project ##
    'cd /path/to/your/workspace'
    'git clone https://akhilb1801@bitbucket.org/akhilb1801/its.git'
    'virtualenv  git / name of virtual environment'
    'cd to virtual enviroment'
    'source bin/activate' -- To activate the Virtual Enviroment
    'sudo pip install -r requirements.txt' --To install requirements in Virtual Enviroment
    Configure database in your Phpmyadmin using zip file
    Update Settings.py file in project according to MySql settings
    'python manage.py migrate'
    'python manage.py runserver'